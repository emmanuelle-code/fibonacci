def fibonacci(n, values = {1: 0, 2: 1}):
    if n in values:
        return values[n]
    else:
        values[n] = fibonacci(n - 2, values) + fibonacci(n - 1, values)
        return values[n]